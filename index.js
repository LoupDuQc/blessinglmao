
let Discord = require('discord.js');
let config = require('./config.json');

let client = new Discord.Client();

client.on('ready', () => {
	console.log('I am ready!');
});

//Lmaoify import and setup
var fs = require('fs').promises;
var Canvas = require('canvas');
Canvas.registerFont('./KanjyukuGothic.otf', { family: 'Not Comic Sans' });
var Image = Canvas.Image;
var lmaoTemplate = new Image();
lmaoTemplate.src = './lmaoTemplate.png';
var canvas = Canvas.createCanvas(128, 128);
var ctx = canvas.getContext('2d')

var templateAvailibleSizes = { width: 100, height: 50 };

/**
 * 
 * @param {string} text 
 * @param {string} fileName 
 */
async function Lmaoify(text, fileName) {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	ctx.drawImage(lmaoTemplate, 0, 0, canvas.width, canvas.height);

	ctx.fillStyle = 'black';
	ctx.textAlign = 'center';
	let fontSize = 22;
	ctx.font = `${fontSize}pt Not Comic Sans`;

	let textSizes = ctx.measureText(text);
	if (text.includes(" ")) {
		let textParts = text.split(" ");
		//todo: check the width of each segment instead of global
		let maxWidth = Math.max(...textParts.map((segment) => ctx.measureText(segment).width));
		while ((textSizes.emHeightAscent * textParts.length > templateAvailibleSizes.height || maxWidth > templateAvailibleSizes.width) && fontSize != 1) {
			textSizes = ctx.measureText(text);
			maxWidth = Math.max(...textParts.map((segment) => ctx.measureText(segment).width));
			fontSize -= 1;
			ctx.font = `${fontSize}pt Not Comic Sans`;
		}
		textParts.forEach((part, index) => {
			ctx.fillText(part, 64, 64 - (textSizes.emHeightAscent * textParts.length) / 2 + textSizes.emHeightAscent * index);
		});
	} else {
		while (textSizes.width > templateAvailibleSizes.width && fontSize != 1) {
			textSizes = ctx.measureText(text);
			fontSize -= 1;
			ctx.font = `${fontSize}pt Not Comic Sans`;
		}
		ctx.fillText(text, 64, 64);
	}	
	const buffer = canvas.toBuffer('image/png')
  	await fs.writeFile(`./${fileName}.png`, buffer)
}

client.on('message', async message => {
	if (message.content.includes("!lmao")) {
		let text = message.content.replace("!lmao ", "");
		await Lmaoify(text, message.id);
		await message.reply({ file: `./${message.id}.png` });
		fs.unlink(`./${message.id}.png`);
	}
});

client.login(config.loginToken);
